/* 
 * File:   libjson.h
 * Author: Fcten
 *
 * Created on 2015年5月6日, 上午10:24
 */

#ifndef __LIBJSON_H__
#define	__LIBJSON_H__

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef	NULL
#define	NULL 0
#endif

typedef struct json_str_s {
    unsigned int len;
    char * str;
    struct json_str_s * parent;
} json_str_t;

typedef struct {
    char * err_msg;
    unsigned int count;
    unsigned int status;
    char * str;
    unsigned int len;
    void (*callback)( json_str_t *path, json_str_t *value );
} json_task;

int json_parser( json_task *task );
void json_err_psotion( json_task *task, int *line, int *row );

#ifdef	__cplusplus
}
#endif

#endif	/* __LIBJSON_H__ */

